# Project moved
This project has been moved here: https://gitlab.engr.illinois.edu/engrit-epm/get-appsupersedence  
The reason is because it was updated to be more flexible, accepting a list of application names directly, or a collection name, instead of just a TS name.  